package com.hsogoo.point.service;


import org.springframework.stereotype.Service;

/**
 * Created by hsogoo on 2017/10/23.
 */
@Service
public class PointServiceImpl implements PointService {

    @Override
    public void sendUserPoint(Integer amount) {
        System.out.printf("PointServiceImpl.sendUserPoint()");
    }
}
