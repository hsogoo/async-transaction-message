package com.hsogoo.point.service;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hsogoo.message.impl.robbitMq.AbstractRabbitMqReceiver;

/**
 * Created by hsogoo on 2018/5/30.
 */
@Service
@RabbitListener(queues = "message-send-demo")
public class PointMessageReceiver {

    @Autowired
    private PointService pointService;

    @RabbitHandler
    public void receive(String message) {
        System.out.println(message);
        pointService.sendUserPoint(100);
    }
}
