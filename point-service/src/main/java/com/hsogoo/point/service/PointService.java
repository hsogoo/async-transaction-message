package com.hsogoo.point.service;

import org.springframework.stereotype.Service;

/**
 * Created by hsogoo on 2017/10/23.
 */
public interface PointService {

    public void sendUserPoint(Integer amount);

}
