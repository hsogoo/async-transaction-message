package com.hsogoo.order.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ProtocolConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.config.spring.AnnotationBean;

/**
 * 参数可以通过配置中心配置
 * 这里先写死
 */
@Configuration
public class DubboConfig {

//    @Value("${dubbo.application.name}")
    private String applicationName = "order-service";

//    @Value("${dubbo.registry.address}")
    private String registryAddress = "127.0.0.1";

//    @Value("${dubbo.registry.protocol}")
    private String registryProtocol = "zookeeper";

//    @Value("${dubbo.registry.group}")
    private String registryGroup = "message";

//    @Value("${dubbo.protocol.name}")
    private String protocolName = "dubbo";

//    @Value("${dubbo.protocol.port}")
    private int protocolPort = 20881;

    @Bean
    public ApplicationConfig applicationConfig() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName(applicationName);
        return applicationConfig;
    }

    @Bean
    public RegistryConfig registry() {
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(registryAddress);
        registryConfig.setProtocol(registryProtocol);
        return registryConfig;
    }

    @Bean
    public ProtocolConfig protocolConfig() {
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setName(protocolName);
        protocolConfig.setPort(protocolPort);
        return protocolConfig;
    }

    /**
     * 因为使用注解方式，所以需要配置扫描的包
     * @return
     */
    @Bean
    public static AnnotationBean annotationBean() {
        AnnotationBean annotationBean = new AnnotationBean();
        annotationBean.setPackage("com.hsogoo.order");
        return annotationBean;
    }


}