package com.hsogoo.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by hsogoo on 2017/10/23.
 */
@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private MessageService messageService;

    @Override
    public String createOrder() {
        System.out.println("OrderServiceImpl.createOrder()");

        messageService.sendMessage();

        return "orderCode:123456";
    }
}
