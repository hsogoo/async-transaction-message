package com.hsogoo.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hsogoo.message.MqSender;

/**
 * Created by za-huangsaigang on 2018/5/31.
 */
@Service
public class MessageService {

    @Autowired
    private MqSender rabbitMqSender;

    public void sendMessage(){
        rabbitMqSender.sendMessage("messag-send-demo-exchange", "test-bingding", "helloWorld");
    }
}
