package com.hsogoo.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hsogoo.order.service.MessageService;
import com.hsogoo.order.service.OrderService;

/**
 * Created by hsogoo on 2017/10/24.
 */
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;


    @RequestMapping(value = "createOrder", method = { RequestMethod.GET })
    public String createOrder(){
        return orderService.createOrder();
    }
}
