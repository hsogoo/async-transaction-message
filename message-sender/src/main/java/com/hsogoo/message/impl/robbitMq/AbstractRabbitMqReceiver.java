package com.hsogoo.message.impl.robbitMq;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;

import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by hsogoo on 2018/5/31.
 */
@Slf4j
public abstract class AbstractRabbitMqReceiver implements ChannelAwareMessageListener {

    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        String msg = new String(message.getBody());

        log.info("==> consume message begin, req:{}", msg);
        try {
            consume(msg);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Throwable t) {
            log.error("==> failed to consume message", t);
        }
    }

    public abstract void consume(String message);

}
