package com.hsogoo.message;

public interface MqReceiver {

    public void receive(String message);

}
