package com.hsogoo.message.bean;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * create by hsogoo at 2018/5/30 0030 下午 23:56
 */
@Data
public class MessageBean implements Serializable {

    private Long id;

    private String exchange;

    private String routingKey;

    private String message;

    private String status;

    private Date createTime;

    private Date updateTime;

    private MessageBean(Builder builder) {
        this.exchange = builder.exchange;
        this.routingKey = builder.routingKey;
        this.message = builder.message;
        this.status = builder.status;
        this.createTime = builder.createTime;
        this.updateTime = builder.updateTime;
    }

    public static class Builder {

        private Long id;

        private String exchange;

        private String routingKey;

        private String message;

        private String status;

        private Date createTime;

        private Date updateTime;

        public Builder() {

        }

//        public Builder(String exchange, String routingKey, String message) {
//            this.exchange = exchange;
//            this.routingKey = routingKey;
//            this.message = message;
//        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder exchange(String exchange) {
            this.exchange = exchange;
            return this;
        }

        public Builder routingKey(String routingKey) {
            this.routingKey = routingKey;
            return this;
        }

        public Builder message(String message) {
            this.status = message;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder createTime(Date createTime) {
            this.createTime = createTime;
            return this;
        }

        public Builder updateTime(Date updateTime) {
            this.updateTime = updateTime;
            return this;
        }

        public MessageBean build() {
            return new MessageBean(this);
        }

    }

}
