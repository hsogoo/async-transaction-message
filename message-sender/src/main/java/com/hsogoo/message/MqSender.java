package com.hsogoo.message;

public interface MqSender {

    public void sendTxMessage(String exchange, String routingKey, String message);

    public void sendMessage(String exchange, String routingKey, String message);

}
